#Some aliases to include to ~.bashrc

#include some filesystem specific aliases

if [ -f ~/.daylan_aliases ]; then
    . ~/.daylan_aliases
fi

alias gettiame='sudo apt-get install'
alias hh='history'
alias hhg='history | grep'
alias xlogout='xfce4-session-logout'
alias cd..='cd ..'
alias cd-='cd -'

alias gti='git'
alias gitcommit='git commit'
alias rmr='rm -r'
alias valgrindFullLeakCheck='valgrind --leak-check=full'
alias sqlConnect='mysql -h localhost -u root -p'
alias lock-screen='i3lock -ti ~/Pictures/Commodore_8580_die_20x_top_8000w.png'
