
# Dbus-bang

Simple utility that allow you to provide one or more commands to be executed with a combination of keys.
Only for linux environments using [dbus](https://www.freedesktop.org/wiki/Software/dbus/)

### Installation:

Make a symlink of the `dbus-bang` script somewhere on your `$PATH`, for example:
```bash
ln -s ./dbus-bang ~/.local/bin/bang
```

On your desktop environment configuration associate some key combination with the execution of the command: `signal_dbus_bang`

If you want to hear a bell when jobs are done install 'playsound' python library.

### Usage:

On a terminal just type:
```bash
bang "<command with arguments>" ["<command with arguments>" ... ]
```

Press your selected key convination in order to launch the given commands

#### Credits:

Bell sound by [InspectorJ](https://freesound.org/people/InspectorJ/sounds/415510/)
