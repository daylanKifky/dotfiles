## A list of configuration files

Controled with [*stow*](https://www.gnu.org/software/stow/)

..or just by creating symlinks arround..

### Contains:
-__tmux__ colored and custom commands

-__script__ a list of personal commonly used commands 

-__git__ git conf

-__dbus-bang__ simple utility that allow you to provide one or more commands to be executed with a combination of keys.

-__zsh__ Zshell conf

-__oh_my_zsh__ config to use [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh), and custom theme based on [avit](https://github.com/robbyrussell/oh-my-zsh/blob/master/themes/avit.zsh-theme)


(icon by mantisshrimpdesign from thenounproject)